﻿using System;
using System.Diagnostics;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\n\nEnter the total number of elements: ");
            int max = Convert.ToInt32(Console.ReadLine());
            string [] numarray = new string[1];

        Found:
            numarray = Console.ReadLine().ToString().Split(',');

            if (numarray.Length == max)
            {
                int n = max;

                Stopwatch sw2 = new Stopwatch();
                sw2.Start();

                for (int i = 1; i < max; i++)
                {
                    for (int j = 0; j < max - i; j++)
                    {
                        if (int.Parse(numarray[j]) > int.Parse(numarray[j + 1]))
                        {
                            int temp = int.Parse(numarray[j]);
                            numarray[j] = numarray[j + 1];
                            numarray[j + 1] = temp.ToString();
                        }
                    }
                }

                Console.Write("\n\nThe numbers in ascending orders are given below: \n\n");
                for (int i = 0; i < max; i++)
                {

                    Console.Write(numarray[i] + ",");
                }

                sw2.Stop();
                Console.WriteLine("\n\n Bubble Sort: " + sw2.Elapsed);

                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\n record not the same with maximum number of array selected");
                goto Found;
            }
            Console.ReadLine();
        }
    }
}   

